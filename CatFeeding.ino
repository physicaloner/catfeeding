#include <Servo.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);
Servo myservo;
unsigned long interval = 0;
unsigned long ticker = 0;
unsigned long tmr1000 = 0;
int pinServo = 13;
String zero = "00:";


const int loop_time = 15; // เวลาที่ของการดีเลย์ในการให้อาหาร (วินาที)
const int feed_time = 4; // เวลาที่ปล่อยอาหาร (วินาที)
const int loop_feeding = 1; // จำนวนรอบที่หมุนให้อาหารต่อ 1 ครั้ง

void OpenServo();

void setup()
{
    // initialize the LCD
    Serial.begin(9600);
    lcd.begin();
    lcd.setCursor(0, 0);
    // Print a message to the LCD.
    lcd.print("Hello !!!"); //ฟังก์ชั่นในการกำหนดข้อความที่ต้องการแสดงผล
    lcd.setCursor(0, 1); //ฟังก์ชั่นในการกำหนดตำแหน่ง Cursor
    lcd.print("Cat Feeding...");
    lcd.clear();
    myservo.attach(pinServo);
    myservo.write(120);
}

void loop() 
{
    ticker = millis();
    unsigned long time_as_loop = loop_time * 1000;
    unsigned long time_after = (ticker - interval) / 1000;
    
    if(ticker > (tmr1000 + 1000))
    {

    lcd.clear();
    lcd.setCursor(0, 0); //ฟังก์ชั่นในการกำหนดตำแหน่ง Cursor
    lcd.print("Timer :"); //ฟังก์ชั่นในการกำหนดข้อความที่ต้องการแสดงผล
    lcd.setCursor(0, 1); //ฟังก์ชั่นในการกำหนดตำแหน่ง Cursor
      
    if(loop_time >= 3600)
    {
        unsigned long hour = (((interval / 1000) + loop_time) - time_after) / 3600;
        unsigned long minute = ((((interval / 1000) + loop_time) - time_after) / 60) % 60;
        unsigned long sec = (loop_time - (time_after % 60)) % 60;
        lcd.print(String(hour) + ":" + String(minute) + ":" + String(sec)); //ฟังก์ชั่นในการกำหนดข้อความที่ต้องการแสดงผล
        Serial.print("hour : ");
        Serial.println((loop_time / 3600));
        Serial.print("hour real : ");
        Serial.println(time_after);
    }
    else if(loop_time >= 60 && loop_time < 3600)
    {
        unsigned long minute = (loop_time / 60) - ( 1 + (time_after / 60));
        unsigned long sec = (loop_time - (time_after % 60)) % 60;
        lcd.print(zero + String(minute) + ":" + String(sec)); //ฟังก์ชั่นในการกำหนดข้อความที่ต้องการแสดงผล
    }
    else
    {
        unsigned long sec = loop_time - (time_after % 60);
        lcd.print(zero + zero + String(sec)); //ฟังก์ชั่นในการกำหนดข้อความที่ต้องการแสดงผล  
    }

        Serial.print("ticker : ");
        Serial.println((ticker / 1000));
        Serial.print("interval : ");
        Serial.println(((interval / 1000) + loop_time));
        Serial.println("");
        tmr1000 = ticker;
    }

    if((ticker / 1000) > ((interval / 1000) + loop_time))
    {
        OpenServo();
        interval = ticker + (((feed_time * 1000) * loop_feeding) + (500 * loop_feeding));
    }
}

void OpenServo()
{
    lcd.clear();
    lcd.setCursor(0, 0); //ฟังก์ชั่นในการกำหนดตำแหน่ง Cursor
    lcd.print("Cat Feeding !!!"); //ฟังก์ชั่นในการกำหนดข้อความที่ต้องการแสดงผล
    myservo.write(120);
    for(int i = 0; i < loop_feeding; i++)
    {
      delay(500);
      myservo.write(0);
      delay(feed_time * 1000);
      myservo.write(120);
    }
}
